<?php

namespace Tests\Unit;

use Orchestra\Testbench\TestCase;
use Illuminate\Auth\SessionGuard;

class TemplateManagerTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Template\TemplateServiceProvider',
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            "TemplateManager" => "Dendev\\Template\\TemplateManagerFacade",
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
       // $config = include './tests/config.php';
       // $mysql_connection = $config['db']['mysql'];

       // $app['config']->set('database.default', 'mysql');
       // $app['config']->set('database.connections.mysql', $mysql_connection);
       // $app['config']->set('auth.providers.users.model', 'Dendev\Template\app\Models\User');
    }

    //
    public function testFromDate()
    {
        $exist = \TemplateManager::test_me();
        $this->assertTrue($exist);
    }
}

