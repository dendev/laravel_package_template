<?php

namespace Dendev\Template;

use Illuminate\Support\Facades\Facade;

class TemplateManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'template_manager';
    }
}
